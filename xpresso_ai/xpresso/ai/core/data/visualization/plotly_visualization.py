"""Class for visualization of attribute metrics"""

__all__ = ["PlotlyVisualization"]
__author__ = ["Ashritha Goramane"]

import plotly.io as pio

from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    PLOTLY_ORCA_PORT
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.abstract_visualization import \
    PlotType
from xpresso.ai.core.data.visualization.base_visualization import \
    BaseVisualization
from xpresso.ai.core.data.visualization.plotly import quartile, heatmap, bar, \
    scatter, pie
from xpresso.ai.core.data.visualization.report import ScatterReport
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is put here intentionally. we want plotly to use same port always
pio.orca.config.port = PLOTLY_ORCA_PORT


class PlotlyVisualization(BaseVisualization):
    """
    Visualization class takes a automl object and provide functions
    to render plots on the metrics of attribute
    Args:
        dataset(:obj StructuredDataset): Structured dataset object on
            which visualization to be performed
    """

    def __init__(self, dataset):
        super().__init__(dataset)
        self.dataset = dataset
        self.attribute_info = dataset.info.attributeInfo
        self.metric = dataset.info.metrics
        self.logger = XprLogger()
        self.pie_plot = pie
        self.quartile_plot = quartile
        self.bar_plot = bar
        self.heatmap_plot = heatmap
        self.scatter_plot = scatter

    def plot(self, field=None, attr=None, plot_type=None,
             output_format='html', others_percent_threshold=None,
             bar_plot_tail_threshold=None):

        """Renders specific plot for field in metrics depending upon the type
        of plot
        field(str): Metric field of the data to be plotted
        attr(:obj:AttributeInfo): Attribute of which plot to be generated
        plot_type (:obj: `PlotType`): Specifies the type of plot
            to be generated
        output_format (str): html, png or pdf plots to be generated
        others_percent_threshold(int): Optional threshold to club small
            categories in pie chart
        bar_plot_tail_threshold(int): Optional threshold to club tail
            in bar chart"""

        inputs = self.process_input(field=field, attr=attr, plot_type=plot_type)
        if not inputs:
            return False
        plot_file_name, plot_title, input_1, input_2, axes_labels = inputs
        if plot_type == PlotType.QUARTILE.value:
            self.quartile_plot.NewPlot(input_1, output_format=output_format,
                                       auto_render=True,
                                       plot_title=plot_title,
                                       filename=plot_file_name,
                                       axes_labels=axes_labels)
            return True
        elif plot_type == PlotType.PIE.value:
            self.pie_plot.NewPlot(input_1, input_2,
                                  output_format=output_format,
                                  auto_render=True,
                                  plot_title=plot_title,
                                  filename=plot_file_name)
            return True
        elif plot_type == PlotType.BAR.value:
            self.bar_plot.NewPlot(input_1, input_2,
                                  output_format=output_format,
                                  auto_render=True,
                                  plot_title=plot_title,
                                  filename=plot_file_name,
                                  axes_labels=axes_labels)
            return True
        return False

    def target_variable_plot(self, target, attr_name=None,
                             output_format=utils.HTML):
        """
        Overrides helper function to plot all combination of target variable
        plots
        Args:
            target(:str): Target variable name
            attr_name(:str): optional attribute name to plot specific
            attribute vs target plots
            output_format (str): html, png or pdf plots to be generated
        """
        print("Target variable not supported by plotly backend")

    def plot_multivariate(self, corr_df, corr, output_format=utils.HTML):
        """
        Overrides helper function to plot heatmap
        Args:
            corr_df(DataFrame): Dataframe of correlation matrix
            corr(str): correlation coefficient
            output_format(str): output format for the plot (png/html)
        """
        input_1 = corr_df.index.values.tolist()
        input_2 = corr_df.columns.values.tolist()
        input_3 = corr_df.to_numpy().tolist()
        self.heatmap_plot.NewPlot(input_1, input_2, input_3,
                                  output_format=output_format,
                                  auto_render=True, plot_title=corr,
                                  filename=corr)
        return True

    def scatter(self, attribute_x=None, attribute_y=None,
                output_format=utils.HTML, file_name=None,
                output_path=REPORT_OUTPUT_PATH, report=False, target=False):
        """
        Renders scatter plots for the numeric attributes
        Args:
            attribute_x(str): x axis attribute
            attribute_y(str): y axis attribute
            output_format (str): html, png or pdf plots to be generated
            output_path(str): path where the report pdf is to be stored
            file_name(str): File name of the plot to be stored
            report (bool): if true generates a report
            target(str): Name of target variable in dataset
        """
        inputs = self.process_scatter_input(attribute_x, attribute_y,
                                            output_format, report)
        if not inputs:
            return False
        output_format, numeric_attr = inputs
        numeric_attr = self.dataset.info.metrics["numeric_attributes"]
        if attribute_x is not None and attribute_x not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_x))
            return
        if attribute_y is not None and attribute_y not in numeric_attr:
            self.logger.info("{} not in numeric attribute".format(attribute_y))
            return

        if attribute_x is not None and attribute_y is not None:
            axes_labels = self.set_axes_labels(attribute_x, attribute_y)
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {} {}".format(attribute_x,
                                                         attribute_y)
            self.scatter_plot.NewPlot(self.dataset.data[attribute_x],
                                      self.dataset.data[attribute_y],
                                      axes_labels=axes_labels,
                                      auto_render=True,
                                      output_format=output_format,
                                      filename=plot_file_name,
                                      plot_title=plot_title)
        elif attribute_x is not None:
            plots = []
            plot_file_name = "{}_scatter".format(attribute_x)
            plot_title = "Scatter plot for {}".format(attribute_x)
            for attribute_y in numeric_attr:
                if attribute_x == attribute_y:
                    continue
                axes_labels = self.set_axes_labels(attribute_x, attribute_y)
                plots.append(
                    self.scatter_plot.NewPlot(self.dataset.data[attribute_x],
                                              self.dataset.data[attribute_y],
                                              axes_labels=axes_labels,
                                              auto_process=True))
                if len(plots) != 4:
                    continue
                scatter.join(plots=plots, output_format=output_format,
                             filename=plot_file_name,
                             plot_title=plot_title)
                plots = []
            if plots:
                scatter.join(plots=plots, output_format=output_format,
                             filename=plot_file_name,
                             plot_title=plot_title)

        elif attribute_x is None and attribute_y is None:
            for attribute_x in numeric_attr:
                plots = []
                plot_file_name = "{}_scatter".format(attribute_x)
                plot_title = "Scatter plot for {}".format(attribute_x)
                for attribute_y in numeric_attr:
                    if attribute_x == attribute_y:
                        continue
                    axes_labels = self.set_axes_labels(attribute_x, attribute_y)
                    plots.append(
                        self.scatter_plot.NewPlot(
                            self.dataset.data[attribute_x],
                            self.dataset.data[attribute_y],
                            axes_labels=axes_labels,
                            auto_process=True))
                    if len(plots) != 4:
                        continue
                    scatter.join(plots=plots, output_format=output_format,
                                 filename=plot_file_name,
                                 plot_title=plot_title)
                    plots = []
                if plots:
                    scatter.join(plots=plots, output_format=output_format,
                                 filename=plot_file_name,
                                 plot_title=plot_title)
        if report:
            ScatterReport(dataset=self.dataset).create_report(
                output_path=output_path,
                file_name=file_name)
